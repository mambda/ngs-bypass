#include "HeavensGate.hpp"
#include <TlHelp32.h>
#include <map>

namespace HeavensGate
{
	uint64_t GetPEB64( )
	{
		auto ntdll = GetModuleHandleA( "ntdll" );
		auto NtWow64QueryInformationProcess64 = ( decltype( NtQueryInformationProcess ) * )GetProcAddress( ntdll, "NtWow64QueryInformationProcess64" );
		struct PROCESS_BASIC_INFORMATION64
		{
			NTSTATUS ExitStatus;
			uint64_t PebBaseAddress;
			uint64_t AffinityMask;
			KPRIORITY BasePriority;
			uint64_t UniqueProcessId;
			uint64_t InheritedFromUniqueProcessId;
		};

		PROCESS_BASIC_INFORMATION64 pbi = { 0 };
		ULONG ret = 0;
		auto stat = NtWow64QueryInformationProcess64( ( HANDLE )-1, ProcessBasicInformation, &pbi, sizeof( pbi ), &ret );
		if ( stat != 0 )
		{
			Log( "Failed to query our peb." );
			return 0;
		}

		return pbi.PebBaseAddress;
	}

	struct PEB_LDR_DATA64 {
		BYTE       Reserved1[ 8 ];
		uint64_t      Reserved2[ 3 ];
		LIST_ENTRY64 InMemoryOrderModuleList;
	};

	struct PEB64 {
		BYTE                          Reserved1[ 2 ];
		BYTE                          BeingDebugged;
		BYTE                          Reserved2[ 1 ];
		uint64_t                         Reserved3[ 1 ];
		uint64_t				ImageBase;
		uint64_t				Ldr;
		uint64_t  						ProcessParameters;
		uint64_t                         Reserved4[ 3 ];
		uint64_t                         AtlThunkSListPtr;
		uint64_t                         Reserved5;
		ULONG                         Reserved6;
		uint64_t                         Reserved7;
		ULONG                         Reserved8;
		ULONG                         AtlThunkSListPtr32;
		uint64_t                         Reserved9[ 45 ];
		BYTE                          Reserved10[ 96 ];
		uint64_t 						PostProcessInitRoutine;
		BYTE                          Reserved11[ 128 ];
		uint64_t                         Reserved12[ 1 ];
		ULONG                         SessionId;
	};

	struct LDR_DATA_TABLE_ENTRY64 {
		LIST_ENTRY64 InLoadOrderLinks;
		LIST_ENTRY64 InMemoryOrderLinks;
		LIST_ENTRY64 InInitializationOrderLinks;
		uint64_t DllBase;
		PVOID64 EntryPoint;
		PVOID64 Reserved3;

		struct UNICODE_STRING64
		{
			USHORT Length;
			USHORT MaximumLength;
			DWORD PAD;
			PWSTR  Buffer;
		};
		UNICODE_STRING64 FullDllName;
	};

	template <typename t>
	bool read( uint64_t addr, t * buffer ) noexcept
	{
#undef max
		if ( addr < std::numeric_limits<uint32_t>( ).max( ) )
		{
			memcpy( buffer, ( void * )addr, sizeof( t ) );
		}
		else
		{
			// need rpm.
			auto ntdll = GetModuleHandleA( "ntdll" );
			auto NtWow64ReadVirtualMemory64 = ( long( __stdcall * )( HANDLE, uint64_t, void *, uint64_t, uint64_t * ) )GetProcAddress( ntdll, "NtWow64ReadVirtualMemory64" );
			auto handle = OpenProcess( PROCESS_ALL_ACCESS, 0, GetCurrentProcessId( ) );

			uint64_t ret = 0;
			auto stat = NtWow64ReadVirtualMemory64( handle, addr, buffer, sizeof( t ), &ret );
			CloseHandle( handle );
			if ( stat != 0 )
			{
				Log( "Failed to read" );
				return false;
			}
		}

		return true;
	}

	uint64_t GetModule( PEB64 * pPEB, const wchar_t * mod )
	{
		PEB_LDR_DATA64 ldr = { 0 };
		read<PEB_LDR_DATA64>( pPEB->Ldr, &ldr );
		auto cur = ldr.InMemoryOrderModuleList.Flink;
		auto start = CONTAINING_RECORD( cur, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
		//Log( "BASE of %S AT %llx", start->FullDllName.Buffer, start->DllBase );
		LDR_DATA_TABLE_ENTRY64 next = *start;
		do
		{
			auto loc = CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
			read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )loc + next.InMemoryOrderLinks.Flink, &next );
			//next = CONTAINING_RECORD( &next, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );

			if ( wcsstr( next.FullDllName.Buffer, mod ) )
			{
				Log( "BASE of %S AT %llx", next.FullDllName.Buffer, next.DllBase );
				return next.DllBase;
			}

			if ( next.InMemoryOrderLinks.Flink == start->InMemoryOrderLinks.Blink )
				break;
		} while ( true );

		return 0;
	}

	uint64_t GetExport( uint64_t mod, const char * name )
	{
		IMAGE_DOS_HEADER pDos = { 0 };
		read<IMAGE_DOS_HEADER>( mod, &pDos );

		auto nth_pos = pDos.e_lfanew + mod;
		IMAGE_NT_HEADERS64 nth = { 0 };
		read<IMAGE_NT_HEADERS64>( nth_pos, &nth );

		auto exp = nth.OptionalHeader.DataDirectory[ IMAGE_DIRECTORY_ENTRY_EXPORT ];
		IMAGE_EXPORT_DIRECTORY ied = { 0 };
		read<IMAGE_EXPORT_DIRECTORY>( exp.VirtualAddress + mod, &ied );
		uint64_t name_addr = ied.AddressOfNames + mod;
		uint64_t func_addr = ied.AddressOfFunctions + mod;
		uint64_t ord_addr = ied.AddressOfNameOrdinals + mod;
		int i = 0;

		for ( int i = 0; i < ied.NumberOfNames; i++ )
		{
			auto cur_name = name_addr + ( i * 4 );
			uint32_t name_off = 0;
			read<uint32_t>( cur_name, &name_off );
			char name_buffer[ 256 ] = { 0 };
			read<decltype( name_buffer )>( name_off + mod, &name_buffer );


			if ( !_stricmp( name_buffer, name ) )
			{
				auto cur_ord = ord_addr + ( i * 2 );
				uint16_t ord_off = 0;
				read<uint16_t>( cur_ord, &ord_off );

				auto cur_func = func_addr + ( ord_off * 4 );
				uint32_t func_off = 0;
				read<uint32_t>( cur_func, &func_off );
				int x = 0;
				return ( func_off + mod );
			}
		}

		return 0;
	}

	void BPAllThreads( uintptr_t loc )
	{
		long me = GetCurrentThreadId( );
		bool success = true; // lol

		int count = 0;

		THREADENTRY32 te = {};
		te.dwSize = sizeof( te );
		HANDLE hSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, GetCurrentProcessId( ) );
		if ( Thread32First( hSnap, &te ) )
		{
			do
			{
				if ( te.th32OwnerProcessID == GetCurrentProcessId( ) && te.th32ThreadID != me )
				{
					auto tid = te.th32ThreadID;
					HANDLE hThread = OpenThread( THREAD_SET_CONTEXT | THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME | THREAD_QUERY_INFORMATION, false, tid );
					if ( !hThread || hThread == ( HANDLE )-1 )
					{
						LogB( ":(" ); // ignoring errors cause lol
						success = false;
						break;
					}

					if ( SuspendThread( hThread ) == ( DWORD )-1 )
					{
						LogB( "!!!" );
						success = false;
						break;
					}

					CONTEXT ctx = { 0 };
					ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_DEBUG_REGISTERS;

					if ( !GetThreadContext( hThread, &ctx ) )
					{
						Log( "Failed to get context." );
						success = false;
						break;
					}

					ctx.Dr0 = loc;
					ctx.Dr7 |= 1;

					if ( !SetThreadContext( hThread, &ctx ) )
					{
						Log( "Failed to set context." );
						success = false;
						break;
					}

					ResumeThread( hThread );
					count++;
				}

			} while ( Thread32Next( hSnap, &te ) );
		}

		CloseHandle( hSnap );
		LogB( "We breakpointed %d threads", count );
	}

	extern "C" void SetupLongHandler( uint64_t RtlExceptionHandlerFn );
	void Setup64BitExceptionHandler( )
	{
		static bool bOnce = false;
		if ( !bOnce )
		{
			bOnce = true;
			auto peb = ( PEB64 * )GetPEB64( );
			LogB( "PEB IS %llx", peb );
			auto nt = GetModule( peb, L"SYSTEM32\\ntdll" );
			LogB( "NTDLL AT %llx", nt );
			auto exp = GetExport( nt, "RtlAddVectoredExceptionHandler" );
			LogB( "RtlAddVectoredExceptionHandler at %llx", exp );
			LogB( "Attempting to call x64 assembly." );
			SetupLongHandler( exp );
			LogB( "Assembly executed. We havent crashed yet boss." )
		}
	}

	uintptr_t gate_loc = 0;
	std::map< uintptr_t, unsigned char> new_threads;
	LONG  __stdcall NewThreadHandler( EXCEPTION_POINTERS * pPtrs )
	{
		if ( pPtrs->ExceptionRecord->ExceptionCode == EXCEPTION_BREAKPOINT )
		{
			auto cr = pPtrs->ContextRecord;

			auto tid = GetCurrentThreadId( );
			auto match = new_threads.find( cr->Eip );
			if ( match != new_threads.end( ) )
			{
				auto old_byte = match->second;
				//LogB( "0xCC Hit." );
				Utility::SetByte( cr->Eip, old_byte );
				new_threads.erase( match );

				cr->Dr0 = gate_loc;
				cr->Dr7 |= 1;
				return EXCEPTION_CONTINUE_EXECUTION;
			}
		}

		return EXCEPTION_CONTINUE_SEARCH;

	}


	void AddThread( )
	{
		auto tid = GetCurrentThreadId( );
		//LogB( "Got TEB for thread %d [ %p ]", tid, teb );
		uintptr_t given_addr = 0;

		//uintptr_t start_addr = 0;
		auto status = NtQueryInformationThread( ( HANDLE )-2, ThreadQuerySetWin32StartAddress, &given_addr, sizeof( given_addr ), 0 );
		if ( status != STATUS_SUCCESS )
		{
			LogB( "FAILED TO QIT %X", status );
		}

		if ( given_addr )
		{
			Log( "Added thread %d by bping entry %p", tid, given_addr );
			new_threads.insert( { given_addr, *( unsigned char * )given_addr } );
			// trigger an exception to be caught by our handler. Then trigger the debug breakpoints
			Utility::SetByte( given_addr, 0xCC );
		}
	}

	bool HookGates( uintptr_t module_base )
	{
		auto size = Utility::GetModuleSize( module_base );
		//83 04 24 05 CB
		auto loc = Utility::FindPattern( module_base, size, ( char * )"83 04 24 05 CB" ); // +19f795
		if ( loc )
		{
			auto real_gate = Utility::FindPattern( loc, 0x100, ( char * )"FF 55 08" );
			if ( real_gate )
			{
				LogB( "Heavens gate with call found at %p and %p", loc, real_gate );
				HeavensGate::Setup64BitExceptionHandler( );
				gate_loc = real_gate;
				AddVectoredExceptionHandler( TRUE, NewThreadHandler );
				LogB( "New Thread exception handler installed." );

				BPAllThreads( real_gate );
				return true;
			}
		}

		return false;
	}

	hb_callback pCallback = nullptr;
	void SetHeavensBridgeCallback( hb_callback pCallback )
	{
		HeavensGate::pCallback = pCallback;
	}

	uintptr_t ntbase = 0, ntsize = 0;
	uintptr_t MatchSyscallToSymbol( int32_t syscall_index )
	{
		if ( !ntbase )
		{
			ntbase = ( uintptr_t )GetModuleHandleA( "ntdll" );
			ntsize = Utility::GetModuleSize( ntbase );
		}

		// TODO: SigScan regular ntdll and check if we get an ID match.
		char sig[ 100 ] = { 0 };
		auto split_integer = [ syscall_index ]( int byte_number ) -> unsigned char
		{
			// assume we have the number 0x26 and the number 0x100
			// in 0x26, first byte is 26, remaining 3 are 0
			// in 0x100, first byte is 0, second is 10, remaining is 0
			auto out_byte = ( syscall_index >> ( 8 * byte_number ) ) & 0xFF;
			return out_byte;
		};

		auto one = split_integer( 0 );
		auto two = split_integer( 1 );
		auto three = split_integer( 2 );
		auto four = split_integer( 3 );

		sprintf_s( sig, "B8 %X %X %X %X BA 50 8C", one, two, three, four );
		Log( "Searching for signature %s", sig );
		uintptr_t found_pat = 0;
		found_pat = Utility::FindPattern( ntbase, ntsize, sig );
		return found_pat;
	}

	int32_t GetSyscallForEip( uintptr_t eip )
	{
		unsigned int syscall_index = 0xDEADBABE;
		for ( int i = 0; i < 10; i++ )
		{
			auto pos = eip + i;
			if ( *( unsigned char * )pos == 0xB8 )
			{
				syscall_index = *( unsigned int * )( pos + 1 );
				break;
			}
		}

		return syscall_index;
	}

	void DumpSyscallStruct( HeavensGate::CONTEXT64 * pStruct )
	{
		LogB( "RCX = %llx", pStruct->Rcx );
		LogB( "RDX = %llx", pStruct->Rdx );
		LogB( "R8 = %llx", pStruct->R8 );
		LogB( "R9 = %llx", pStruct->R9 );
		LogB( "RSP = %llx", pStruct->Rsp );
	}

	bool SyscallCallback64( int syscall_index, HeavensGate::CONTEXT64 * pCtx )
	{
		auto GetProcessName = [ ]( DWORD pid )
		{
			std::wstring out( L"" );

			auto hndl = OpenProcess( PROCESS_QUERY_INFORMATION, 0, pid );
			if ( hndl && hndl != INVALID_HANDLE_VALUE )
			{
				wchar_t buf[ MAX_PATH ] = { 0 };
				UNICODE_STRING * buf2 = ( UNICODE_STRING * )buf;
				ULONG ret = 0;
				auto stat = NtQueryInformationProcess( hndl, ProcessImageFileName, buf2, sizeof( buf ), &ret );
				CloseHandle( hndl );
				if ( !stat )
				{
					out = std::wstring( buf2->Buffer );
					auto last = out.find_last_of( L'\\' );
					out = out.substr( last + 1 );
					//LogB( "Returning %S", out.c_str( ) );
					return out;
				}
				return out;
			}
			return out;
		};
		bool skipped = false;

		auto skip_call = [ pCtx , &skipped ]( DWORD ret_value )
		{
			pCtx->Rip += 3; // we havent called yet, so just ditch it loooooool
			pCtx->Rax = ret_value;
			skipped = true;
		};

		if ( syscall_index == 0x26 ) // If i want to be able to catch and modify these on the fly, we would need to return to 32 bit mode and attempt to modify these structures.
		{
			auto pid = ( DWORD )( ( uint64_t * )pCtx->R9 )[ 0 ];
			auto name = GetProcessName( pid );
			if ( !name.empty( ) )
			{
				LogB( "[ZwOpenProcess] PID: %d (%S)", pid, name.c_str( ) );

				if ( wcsstr( name.c_str( ), L"cheat" ) )
				{
					LogB( "Hiding cheat engine!" );
					skip_call( STATUS_ACCESS_DENIED );
				}
			}
			else
			{
				//LogB( "[ZwOpenProcess] Cant get handle/name for PID: %d", pid );
			}
		}
		else if ( syscall_index == 0x23 )
		{
			auto hHandle = ( HANDLE )pCtx->Rcx;
			auto pid = GetProcessId( hHandle );
			auto mod_name = Utility::GetModuleForAddress( pCtx->Rdx, pid );
			LogB( "[ZwQueryVirtualMemory] PID: %d (%S) | Class: %d | Loc: %llx [%S]", pid, GetProcessName( pid ).c_str( ), ( int )pCtx->R8, pCtx->Rdx, mod_name );
		}
		else if ( syscall_index == 0x3F )
		{
			auto handle = ( HANDLE )pCtx->Rcx;
			auto loc = pCtx->Rdx;
			auto pid = GetProcessId( handle );
			auto name = GetProcessName( pid );
			auto mod_name = Utility::GetModuleForAddress( loc, pid );
			LogB( "[ZwReadVirtualMemory] PID %d (%S) at location %llx which is module %S", pid, name.c_str( ), loc, mod_name );
		}
		else if ( syscall_index == 0x19 )
		{
			auto handle = ( HANDLE )pCtx->Rcx;
			auto info = ( long )pCtx->Rdx;
			auto pid = GetProcessId( handle );
			auto name = GetProcessName( pid );
			LogB( "[ZwQueryInformationProcess] PID: %d (%S) | Class: %d", pid, name.c_str( ), ( long )pCtx->Rdx );
			if ( info == ProcessBasicInformation )
			{
				auto peb = ( HeavensGate::PEB64 * )HeavensGate::GetPEB64( );
				// walk PEB and remove our module.
			}
		}
		else
		{
			LogB( "Dump struct for syscall %X", syscall_index );
			DumpSyscallStruct( pCtx );
		}

		return skipped;
	}


	extern "C" void HeavensBridge( HeavensGate::CONTEXT64 * pCtx )
	{
		//LogB( "Context structure at %p, eip : %llx", pCtx, pCtx->Rip );
		// symbol check will never work. Assume its a syscall.
		// perhaps its a manual syscall. check for a 0xB8, then get the ID.
		// call    qword ptr [rbp+8]
		auto loc = *( uintptr_t * )( pCtx->Rbp + 8 );
		//LogB( "EIP is %p and loc is %p", (uintptr_t)pCtx->Rip, loc );

		auto syscall_index = GetSyscallForEip( loc );
		auto skip = false;
		if ( syscall_index != 0xDEADBABE )
		{
			skip = pCallback( syscall_index, pCtx );
			//skip = SyscallCallback64( syscall_index, pCtx );
		}
		else
		{
			LogB( "Failed to get syscall index for address %p", loc );
			MessageBoxA( 0, "ZOINKS", "JINKEES", 0 );
		}

		if ( !skip )
		{
			pCtx->Rsp -= 8;
			auto ret = pCtx->Rip + 3;
			*( uint64_t * )pCtx->Rsp = ret;
			//LogB( "Set return to %p", ret );
			pCtx->Rip = loc; // not going to overwrite my 0xCC.
		}
	}

}